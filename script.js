console.log(`DOM MANIPULATION`)
//finding html elements
console.log(document.getElementById("demo"))
console.log(document.getElementsByTagName("h1"))
console.log(document.getElementsByClassName("title"))

console.log(document.querySelector("#demo"))
console.log(document.querySelector("h1"))
console.log(document.querySelector(".title"))

//changing html elements
	//property
		//element.innerHTML = new html content
		let myH1 = document.querySelector(".title")
		myH1.innerHTML = `Hello world`
		console.log(myH1)
		//element.style.property
		document.querySelector("h1").style.color = "red";
	//methods
		//element.attribute = new value
		document.getElementById("demo").setAttribute("class","caption")
		document.getElementById("demo").removeAttribute("class")

//event Listner
//syntax:
	//element.addEventListener("event", cb())

let firstName = document.querySelector('#txt-first-name');
let fullName = document.getElementById('span-full-name');
let lastName = document.getElementById('txt-last-name');

const updateName = () => {
	let txtFirst = firstName.value;
	let txtLast = lastName.value;

	fullName.innerHTML = `${txtFirst} ${txtLast}`
}

firstName.addEventListener("keyup", updateName)
lastName.addEventListener("keyup", updateName)

/*firstName.addEventListener('keyup', (event) => {

	let txtName = event.target.value
	fullName.innerHTML = event.target.value

})

lastName.addEventListener('keyup', (e) => {
	let txtLast = e.target.value
	fullName.innerHTML = `${txtName} ${txtLast}`

})*/

